## Forensics project : Analysis of Malware behavior in pcap file

# students  : # Ilias Akli and Omar Lahbabi

# Professor: #  Mr. Davide Balzarotti


The objective of this project is to parse different Network malwares inside a pcap packet.


The kind of malware we succeded to detect are the following: 

**1.anomalous ip adress**

The packet is analyzed. An alert is launched when the bogus or an not allocated ip address is detected. We have a database of these adresses within the folder database.  

**2.icmp unreachable**    

We parsed icmp packets and try to launch an alert once we find that destination is unreachable.

**3.anomalous values of dns**    

We tried to see if dns requests messages contain some anomalous dns domains that are also inside our database.  

**4.dns tunneling**    

Based on dns packet length we  are able to detect some case of dns tunneling. If length of packet transcend a threehold it means that there is a tunneling  

 **5.exfiltrated informations (username,password)**    

In this case we analysed payload and we try to detect some key word like username ,password if they exist we launch an alert.This method is 
not working for encrypted trafic TLS.   

**information about the project**

There are some pcap tests in the folder pcap_tests. 
All the results of malicious behavior are all writen in log.txt file.  

To compile the file lauch:
     
   ```
    $  g++  main.cpp -lpcap
   ```

An example of the launch of the programm for dns tunneling:
   ```
    $  ./a.out pcap_tests/dns-tunnel-iodine.pcap .
   ```
          

All malicious traffic is written in file log.txt